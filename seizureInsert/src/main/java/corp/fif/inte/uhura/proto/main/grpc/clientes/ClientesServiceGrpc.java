package clientes;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.17.1)",
    comments = "Source: Clientes.proto")
public final class ClientesServiceGrpc {

  private ClientesServiceGrpc() {}

  public static final String SERVICE_NAME = "clientes.ClientesService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<clientes.ClientesRequest,
      clientes.ClientesReply> getObtenerInfoClienteMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "obtenerInfoCliente",
      requestType = clientes.ClientesRequest.class,
      responseType = clientes.ClientesReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<clientes.ClientesRequest,
      clientes.ClientesReply> getObtenerInfoClienteMethod() {
    io.grpc.MethodDescriptor<clientes.ClientesRequest, clientes.ClientesReply> getObtenerInfoClienteMethod;
    if ((getObtenerInfoClienteMethod = ClientesServiceGrpc.getObtenerInfoClienteMethod) == null) {
      synchronized (ClientesServiceGrpc.class) {
        if ((getObtenerInfoClienteMethod = ClientesServiceGrpc.getObtenerInfoClienteMethod) == null) {
          ClientesServiceGrpc.getObtenerInfoClienteMethod = getObtenerInfoClienteMethod = 
              io.grpc.MethodDescriptor.<clientes.ClientesRequest, clientes.ClientesReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "clientes.ClientesService", "obtenerInfoCliente"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  clientes.ClientesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  clientes.ClientesReply.getDefaultInstance()))
                  .setSchemaDescriptor(new ClientesServiceMethodDescriptorSupplier("obtenerInfoCliente"))
                  .build();
          }
        }
     }
     return getObtenerInfoClienteMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ClientesServiceStub newStub(io.grpc.Channel channel) {
    return new ClientesServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ClientesServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ClientesServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ClientesServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ClientesServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class ClientesServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void obtenerInfoCliente(clientes.ClientesRequest request,
        io.grpc.stub.StreamObserver<clientes.ClientesReply> responseObserver) {
      asyncUnimplementedUnaryCall(getObtenerInfoClienteMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getObtenerInfoClienteMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                clientes.ClientesRequest,
                clientes.ClientesReply>(
                  this, METHODID_OBTENER_INFO_CLIENTE)))
          .build();
    }
  }

  /**
   */
  public static final class ClientesServiceStub extends io.grpc.stub.AbstractStub<ClientesServiceStub> {
    private ClientesServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ClientesServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ClientesServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ClientesServiceStub(channel, callOptions);
    }

    /**
     */
    public void obtenerInfoCliente(clientes.ClientesRequest request,
        io.grpc.stub.StreamObserver<clientes.ClientesReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getObtenerInfoClienteMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ClientesServiceBlockingStub extends io.grpc.stub.AbstractStub<ClientesServiceBlockingStub> {
    private ClientesServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ClientesServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ClientesServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ClientesServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public clientes.ClientesReply obtenerInfoCliente(clientes.ClientesRequest request) {
      return blockingUnaryCall(
          getChannel(), getObtenerInfoClienteMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ClientesServiceFutureStub extends io.grpc.stub.AbstractStub<ClientesServiceFutureStub> {
    private ClientesServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ClientesServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ClientesServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ClientesServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<clientes.ClientesReply> obtenerInfoCliente(
        clientes.ClientesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getObtenerInfoClienteMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_OBTENER_INFO_CLIENTE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ClientesServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ClientesServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_OBTENER_INFO_CLIENTE:
          serviceImpl.obtenerInfoCliente((clientes.ClientesRequest) request,
              (io.grpc.stub.StreamObserver<clientes.ClientesReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ClientesServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ClientesServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return clientes.Clientes.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ClientesService");
    }
  }

  private static final class ClientesServiceFileDescriptorSupplier
      extends ClientesServiceBaseDescriptorSupplier {
    ClientesServiceFileDescriptorSupplier() {}
  }

  private static final class ClientesServiceMethodDescriptorSupplier
      extends ClientesServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ClientesServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ClientesServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ClientesServiceFileDescriptorSupplier())
              .addMethod(getObtenerInfoClienteMethod())
              .build();
        }
      }
    }
    return result;
  }
}
