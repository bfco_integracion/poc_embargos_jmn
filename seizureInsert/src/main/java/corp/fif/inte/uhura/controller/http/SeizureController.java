package corp.fif.inte.uhura.controller.http;

import corp.fif.inte.uhura.entity.Seizure;
import corp.fif.inte.uhura.entity.ResponseSeizure;
import corp.fif.inte.uhura.repository.SeizureRepository;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import java.sql.SQLException;

@Controller("/seizure")
public class SeizureController {
    private final SeizureRepository seizureRepository;

    public SeizureController(SeizureRepository seizureRepository) {
        this.seizureRepository = seizureRepository;
    }


    @Post("/seizure")
    public ResponseSeizure insertSeizure(@Body Seizure seizure) throws SQLException {
        return seizureRepository.insertSeizure(seizure);
    }

    @Get("/")
    public ResponseSeizure insertSeizure() throws SQLException {
        return seizureRepository.insertSeizure();
    }
}
