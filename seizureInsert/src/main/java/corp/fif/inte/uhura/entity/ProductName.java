package corp.fif.inte.uhura.entity;

import io.micronaut.core.annotation.Creator;
import javax.persistence.Entity;

@Entity
public class ProductName {

    private String productID;
    private String productState;

    @Creator
    public ProductName(String productID, String productState) {
        this.productID = productID;
        this.productState = productState;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductState() {
        return productState;
    }

    public void setProductState(String productState) {
        this.productState = productState;
    }
}
