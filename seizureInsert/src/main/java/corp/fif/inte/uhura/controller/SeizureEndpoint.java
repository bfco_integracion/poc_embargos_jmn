package corp.fif.inte.uhura.controller;

import corp.fif.inte.uhura.service.SeizureService;

import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import seizure.SeizureReply;
import seizure.SeizureRequest;
import seizure.SeizureServiceGrpc;

import javax.inject.Singleton;

@Singleton
public class SeizureEndpoint extends SeizureServiceGrpc.SeizureServiceImplBase {
    private final SeizureService seizureService;

    public SeizureEndpoint(SeizureService seizureService) {
        this.seizureService = seizureService;
    }

    @Override
    public void insertSeizure(SeizureRequest request, StreamObserver<SeizureReply> responseObserver) {

        final SeizureReply reply;

        try {
            reply = seizureService.insertSeizure(request);
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }

    }
}
