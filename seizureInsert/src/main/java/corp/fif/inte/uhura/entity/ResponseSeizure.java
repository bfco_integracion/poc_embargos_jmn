package corp.fif.inte.uhura.entity;

import io.micronaut.core.annotation.Creator;

import javax.persistence.Entity;


@Entity
public class ResponseSeizure {

    private Integer id;
    private String codigoError;
    private String mensajeError;

    @Creator
    public ResponseSeizure(Integer id, String codigoError, String mensajeError){
        this.id = id;
        this.codigoError = codigoError;
        this.mensajeError = mensajeError;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(String codigoError) {
        this.codigoError = codigoError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }



}
