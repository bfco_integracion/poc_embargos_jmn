package corp.fif.inte.uhura.entity;

import io.micronaut.core.annotation.Creator;

import javax.persistence.Entity;

@Entity
public class SeizeInfo {

    private int concept;
    private int seizureType;
    private int debitForm;
    private String receptionDate;
    private String processNumber;
    private int  amount;
    private String city;
    private String address;
    private String judicialAccount;
    private String entity;
    private String seizureDate;
    private Client plaintiff;

    @Creator
    public SeizeInfo(int concept, int seizureType, int debitForm, String receptionDate, String processNumber, int amount, String city, String address, String judicialAccount, String entity, String seizureDate, Client plaintiff) {
        this.concept = concept;
        this.seizureType = seizureType;
        this.debitForm = debitForm;
        this.receptionDate = receptionDate;
        this.processNumber = processNumber;
        this.amount = amount;
        this.city = city;
        this.address = address;
        this.judicialAccount = judicialAccount;
        this.entity = entity;
        this.seizureDate = seizureDate;
        this.plaintiff = plaintiff;
    }

    public int getConcept() {
        return concept;
    }

    public void setConcept(int concept) {
        this.concept = concept;
    }

    public int getSeizureType() {
        return seizureType;
    }

    public void setSeizureType(int seizureType) {
        this.seizureType = seizureType;
    }

    public int getDebitForm() {
        return debitForm;
    }

    public void setDebitForm(int debitForm) {
        this.debitForm = debitForm;
    }

    public String getReceptionDate() {
        return receptionDate;
    }

    public void setReceptionDate(String receptionDate) {
        this.receptionDate = receptionDate;
    }

    public String getProcessNumber() {
        return processNumber;
    }

    public void setProcessNumber(String processNumber) {
        this.processNumber = processNumber;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJudicialAccount() {
        return judicialAccount;
    }

    public void setJudicialAccount(String judicialAccount) {
        this.judicialAccount = judicialAccount;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getSeizureDate() {
        return seizureDate;
    }

    public void setSeizureDate(String seizureDate) {
        this.seizureDate = seizureDate;
    }

    public Client getPlaintiff() {
        return plaintiff;
    }

    public void setPlaintiff(Client plaintiff) {
        this.plaintiff = plaintiff;
    }
}
