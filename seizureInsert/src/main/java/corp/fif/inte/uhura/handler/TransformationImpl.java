package corp.fif.inte.uhura.handler;

import corp.fif.inte.uhura.entity.*;
import seizure.SeizureReply;
import seizure.SeizureRequest;

import javax.inject.Singleton;

@Singleton
public class TransformationImpl implements Transformations {

    @Override
    public Seizure grpcToSoapRequest(SeizureRequest requestGrpc) {

        ProductName productName = new ProductName(requestGrpc.getProduct().getProductID(), requestGrpc.getProduct().getProductState());

        DocumentInfo documentInfo = new DocumentInfo(
                //String documentType
                requestGrpc.getSeizureInformation().getPlaintiff().getDocument().getDocumentType(),
                //String documentNumber
                requestGrpc.getSeizureInformation().getPlaintiff().getDocument().getDocumentNumber()
        );

        Client plaintiff = new Client(
                                    //String firstName
                                   requestGrpc.getSeizureInformation().getPlaintiff().getFirstName() ,
                                   //String middleName
                                   requestGrpc.getSeizureInformation().getPlaintiff().getMiddleName()  ,
                                   //String surname
                                   requestGrpc.getSeizureInformation().getPlaintiff().getSurname() ,
                                   //String lastSurname
                                    requestGrpc.getSeizureInformation().getPlaintiff().getLastSurname(),
                                    documentInfo);

        SeizeInfo seizeInfo = new SeizeInfo(
                                    //int concept
                                    requestGrpc.getSeizureInformation().getConcept(),
                                    //int seizureType
                                    requestGrpc.getSeizureInformation().getSeizureType(),
                                    //int debitForm
                                    requestGrpc.getSeizureInformation().getDebitForm(),
                                    //String receptionDate
                                    requestGrpc.getSeizureInformation().getReceptionDate(),
                                    //String processNumber
                                    requestGrpc.getSeizureInformation().getProcessNumber(),
                                    //int amount
                                    requestGrpc.getSeizureInformation().getAmount(),
                                    // String city
                                    requestGrpc.getSeizureInformation().getCity(),
                                    //String address
                                    requestGrpc.getSeizureInformation().getAddress(),
                                    //String judicialAccount
                                    requestGrpc.getSeizureInformation().getJudicialAccount(),
                                    //String entity
                                    requestGrpc.getSeizureInformation().getEntity(),
                                    //String seizureDate
                                    requestGrpc.getSeizureInformation().getSeizureDate(),
                                    //Client plaintiff
                                    plaintiff
        );

        Client customer = new Client(
                //String firstName
                requestGrpc.getCustomer().getFirstName() ,
                //String middleName
                requestGrpc.getCustomer().getMiddleName()  ,
                //String surname
                requestGrpc.getCustomer().getSurname() ,
                //String lastSurname
                requestGrpc.getCustomer().getLastSurname(),
                documentInfo);

        Seizure seizure = new Seizure(
                //Integer id,
                requestGrpc.getId(),
                //Client customer,
                customer,
                //SeizeInfo seizureInformation,
                seizeInfo,
                //ProductName product
                productName
        );

        return seizure;
    }

    @Override
    public SeizureReply soapToGrpcResponse(ResponseSeizure responseSoap) {

        SeizureReply.Builder seizureReply = SeizureReply.newBuilder();
        seizureReply.setId(responseSoap.getId());
        String codigoError = responseSoap.getCodigoError();
        if (codigoError == null) codigoError = "";
        seizureReply.setCodigoError(codigoError);
        String mensajeError = responseSoap.getMensajeError();
        if(mensajeError == null) mensajeError = "";
        seizureReply.setMensajeError(mensajeError);
        return seizureReply.build();


    }
}
