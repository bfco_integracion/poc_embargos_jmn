package corp.fif.inte.uhura;

import java.sql.SQLException;

import javax.inject.Singleton;

import corp.fif.inte.uhura.entity.ResponseSeizure;
import corp.fif.inte.uhura.repository.SeizureRepository;
import io.micronaut.core.annotation.TypeHint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.context.event.StartupEvent;
import io.micronaut.runtime.Micronaut;
import io.micronaut.runtime.event.annotation.EventListener;

@Singleton
//@TypeHint(typeNames = {"oracle.jdbc.driver.OracleDriver"})
public class Application {
    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    private final SeizureRepository seizureRepository;

    Application(SeizureRepository seizureRepository) {
        this.seizureRepository = seizureRepository;
    }

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }

    @EventListener
    void init(StartupEvent event) {
        if (LOG.isInfoEnabled()) {
            LOG.info("Populating data");
        }
        /*
        * Codigo inicializado en el singleton
        * Solamente se ejecuta una vez esta insercion
        * */
        try {
            ResponseSeizure responseSeizure = seizureRepository.insertSeizure();
            System.out.println("getId " +  responseSeizure.getId());
            System.out.println("getCodigoError " +  responseSeizure.getCodigoError());
            System.out.println("getMensajeError " +  responseSeizure.getMensajeError());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}