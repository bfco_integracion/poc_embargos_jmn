package seizure;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.17.1)",
    comments = "Source: Seizure.proto")
public final class SeizureServiceGrpc {

  private SeizureServiceGrpc() {}

  public static final String SERVICE_NAME = "seizure.SeizureService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<seizure.SeizureRequest,
      seizure.SeizureReply> getInsertSeizureMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "insertSeizure",
      requestType = seizure.SeizureRequest.class,
      responseType = seizure.SeizureReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<seizure.SeizureRequest,
      seizure.SeizureReply> getInsertSeizureMethod() {
    io.grpc.MethodDescriptor<seizure.SeizureRequest, seizure.SeizureReply> getInsertSeizureMethod;
    if ((getInsertSeizureMethod = SeizureServiceGrpc.getInsertSeizureMethod) == null) {
      synchronized (SeizureServiceGrpc.class) {
        if ((getInsertSeizureMethod = SeizureServiceGrpc.getInsertSeizureMethod) == null) {
          SeizureServiceGrpc.getInsertSeizureMethod = getInsertSeizureMethod = 
              io.grpc.MethodDescriptor.<seizure.SeizureRequest, seizure.SeizureReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "seizure.SeizureService", "insertSeizure"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seizure.SeizureRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  seizure.SeizureReply.getDefaultInstance()))
                  .setSchemaDescriptor(new SeizureServiceMethodDescriptorSupplier("insertSeizure"))
                  .build();
          }
        }
     }
     return getInsertSeizureMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SeizureServiceStub newStub(io.grpc.Channel channel) {
    return new SeizureServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SeizureServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SeizureServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SeizureServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SeizureServiceFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static abstract class SeizureServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void insertSeizure(seizure.SeizureRequest request,
        io.grpc.stub.StreamObserver<seizure.SeizureReply> responseObserver) {
      asyncUnimplementedUnaryCall(getInsertSeizureMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getInsertSeizureMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                seizure.SeizureRequest,
                seizure.SeizureReply>(
                  this, METHODID_INSERT_SEIZURE)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class SeizureServiceStub extends io.grpc.stub.AbstractStub<SeizureServiceStub> {
    private SeizureServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SeizureServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SeizureServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SeizureServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void insertSeizure(seizure.SeizureRequest request,
        io.grpc.stub.StreamObserver<seizure.SeizureReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getInsertSeizureMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class SeizureServiceBlockingStub extends io.grpc.stub.AbstractStub<SeizureServiceBlockingStub> {
    private SeizureServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SeizureServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SeizureServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SeizureServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public seizure.SeizureReply insertSeizure(seizure.SeizureRequest request) {
      return blockingUnaryCall(
          getChannel(), getInsertSeizureMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class SeizureServiceFutureStub extends io.grpc.stub.AbstractStub<SeizureServiceFutureStub> {
    private SeizureServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SeizureServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SeizureServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SeizureServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<seizure.SeizureReply> insertSeizure(
        seizure.SeizureRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getInsertSeizureMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_INSERT_SEIZURE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SeizureServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SeizureServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_INSERT_SEIZURE:
          serviceImpl.insertSeizure((seizure.SeizureRequest) request,
              (io.grpc.stub.StreamObserver<seizure.SeizureReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SeizureServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SeizureServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return seizure.SeizureProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SeizureService");
    }
  }

  private static final class SeizureServiceFileDescriptorSupplier
      extends SeizureServiceBaseDescriptorSupplier {
    SeizureServiceFileDescriptorSupplier() {}
  }

  private static final class SeizureServiceMethodDescriptorSupplier
      extends SeizureServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SeizureServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SeizureServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SeizureServiceFileDescriptorSupplier())
              .addMethod(getInsertSeizureMethod())
              .build();
        }
      }
    }
    return result;
  }
}
