package corp.fif.inte.uhura.entity;


import io.micronaut.core.annotation.Creator;

import javax.persistence.Entity;

@Entity
public class DocumentInfo {

    private String documentType;
    private String documentNumber;

    @Creator
    public DocumentInfo(String documentType, String documentNumber) {
        this.documentType = documentType;
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

}
