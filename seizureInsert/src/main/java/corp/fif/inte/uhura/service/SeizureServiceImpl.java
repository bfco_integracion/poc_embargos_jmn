package corp.fif.inte.uhura.service;

import corp.fif.inte.uhura.entity.ResponseSeizure;
import corp.fif.inte.uhura.entity.Seizure;
import corp.fif.inte.uhura.handler.TransformationImpl;
import corp.fif.inte.uhura.repository.SeizureRepository;
import io.grpc.Status;
import io.grpc.StatusException;

import seizure.SeizureReply;
import seizure.SeizureRequest;

import javax.inject.Singleton;
import java.sql.SQLException;


@Singleton
public class SeizureServiceImpl implements SeizureService{

    private final SeizureRepository seizureRepository;

    public SeizureServiceImpl(SeizureRepository seizureRepository) {
        this.seizureRepository = seizureRepository;
    }

    @Override
    public SeizureReply insertSeizure(SeizureRequest request) throws StatusException {

        TransformationImpl t = new TransformationImpl();

        Seizure s = t.grpcToSoapRequest(request);

        ResponseSeizure responseSeizure = null;
        try {
            responseSeizure = seizureRepository.insertSeizure(s);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StatusException(Status.INTERNAL.withDescription("Ha ocurrido un error interno (servicio)"));
        }

        SeizureReply seizureReply = t.soapToGrpcResponse(responseSeizure);

        return seizureReply;
    }

}
