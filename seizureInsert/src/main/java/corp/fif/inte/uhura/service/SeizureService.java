package corp.fif.inte.uhura.service;


import io.grpc.StatusException;
import seizure.SeizureReply;
import seizure.SeizureRequest;

public interface SeizureService {

    public SeizureReply insertSeizure(SeizureRequest request) throws StatusException;

}
