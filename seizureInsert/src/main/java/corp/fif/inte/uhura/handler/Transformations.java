package corp.fif.inte.uhura.handler;

import corp.fif.inte.uhura.entity.ResponseSeizure;
import corp.fif.inte.uhura.entity.Seizure;
import seizure.SeizureReply;
import seizure.SeizureRequest;

public interface Transformations {
    Seizure grpcToSoapRequest(SeizureRequest requestGrpc);
    SeizureReply soapToGrpcResponse(ResponseSeizure responseSoap);
}
