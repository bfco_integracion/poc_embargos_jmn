package corp.fif.inte.uhura.repository;

import corp.fif.inte.uhura.entity.ResponseSeizure;
import corp.fif.inte.uhura.entity.Seizure;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.jdbc.runtime.JdbcOperations;
import io.micronaut.data.model.query.builder.sql.Dialect;

import javax.transaction.Transactional;
import java.sql.*;
import java.util.Calendar;

@JdbcRepository(dialect = Dialect.H2)
public abstract class SeizureRepository {

    private final JdbcOperations jdbcOperations;

    public SeizureRepository(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }


    @Transactional
    public ResponseSeizure insertSeizure() throws SQLException {
        ResultSet resultSet = null;
        CallableStatement callableStatement = null;
        System.out.println("Inicia  insertSeizure()");
        try {
            System.out.println("Ingresa  insertSeizure()");
            Connection c = jdbcOperations.getConnection();

            String nameStoredProcedure = "{call USR_EMBARGOS.PKG_CS_EMBARGO_PROC.EMB_INS_EMBARGOS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

            callableStatement =
                    c.prepareCall(nameStoredProcedure);
            callableStatement.setString(1, "1098717998");
            callableStatement.setInt(2, 1);
            callableStatement.setString(3, "Maria");
            callableStatement.setString(4, "Juliana");
            callableStatement.setString(5, "Perez");
            callableStatement.setString(6, "Fernandez");
            callableStatement.setInt(7, new Integer(1)); //flag
            callableStatement.setInt(8, new Integer(1)); //concepto embargo
            callableStatement.setInt(9, new Integer(2)); //tipo embargo
            callableStatement.setInt(10, new Integer(1)); // forma debito
            callableStatement.setInt(11, new Integer(1)); //tipo medida
            callableStatement.setInt(12, new Integer(122)); //identidad embargo
            callableStatement.setInt(13, new Integer(1)); //edo embargo
            callableStatement.setInt(14, new Integer(2)); //edo carga

            java.sql.Date theDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());

            callableStatement.setDate(15, theDate);//fecha recepcion
            callableStatement.setString(16, "1"); //num oficio
            callableStatement.setString(17, "2"); //num proceso
            callableStatement.setString(18, "999800021"); //cuenta judicial
            callableStatement.setInt(19, new Integer(100000)); //monto
            callableStatement.setString(20, "M"); //flag tipo carga
            callableStatement.setString(21, "calle 123 # 34 -34"); //direccion
            callableStatement.setString(22, "Bogota"); //ciudad
            callableStatement.setString(23, "mlvargas"); //usuario
            callableStatement.setString(24, "Nombre1, Nombre2, Apellido1, Apellido2,1,9687463"); //demandantes
            callableStatement.setString(25, "1,65434234"); //emb prod
            callableStatement.setString(26, null); //representante legal

            callableStatement.registerOutParameter(27, Types.INTEGER);
            callableStatement.registerOutParameter(28, Types.INTEGER);
            callableStatement.registerOutParameter(29, Types.VARCHAR);


            System.out.println("Ejecutando " + nameStoredProcedure);
            resultSet = callableStatement.executeQuery();
            System.out.println("Finaliza ejecucion " + nameStoredProcedure);

            Integer idSeizure = (Integer) callableStatement.getObject(27);
            System.out.println("Respuesta 27 " +  idSeizure);
            String codigoError = (String) callableStatement.getObject(28);
            System.out.println("Respuesta 28 " +  codigoError);
            String mensajeError = (String) callableStatement.getObject(29);
            System.out.println("Respuesta 29 " +  mensajeError);

            //return jdbcOperations.entityStream(resultSet, Pet.class).collect(Collectors.toList());

            final ResponseSeizure responseSeizure = new ResponseSeizure(idSeizure, codigoError, mensajeError);
            return responseSeizure;

        } finally {
            if(resultSet != null) {
                resultSet.close();
            }

            if(callableStatement != null) {
                callableStatement.close();
            }
        }

    }




    @Transactional
    public ResponseSeizure insertSeizure(Seizure seizure) throws SQLException {
        ResultSet resultSet = null;
        CallableStatement callableStatement = null;
        System.out.println("Inicia insertSeizure(Seizure seizure)");
        try {
            System.out.println("Ingresa insertSeizure(Seizure seizure)");
            Connection c = jdbcOperations.getConnection();

            String nameStoredProcedure = "{call USR_EMBARGOS.PKG_CS_EMBARGO_PROC.EMB_INS_EMBARGOS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

            callableStatement =
                    c.prepareCall(nameStoredProcedure);
            callableStatement.setString(1, seizure.getCustomer().getDocument().getDocumentNumber());
            callableStatement.setInt(2, new Integer(seizure.getCustomer().getDocument().getDocumentType()).intValue());
            callableStatement.setString(3, seizure.getCustomer().getFirstName());
            callableStatement.setString(4, seizure.getCustomer().getMiddleName());
            callableStatement.setString(5, seizure.getCustomer().getSurname());
            callableStatement.setString(6, seizure.getCustomer().getLastSurname());
            callableStatement.setInt(7, new Integer(1)); //flag
            callableStatement.setInt(8, seizure.getSeizureInformation().getConcept()); //concepto embargo
            callableStatement.setInt(9, seizure.getSeizureInformation().getSeizureType()); //tipo embargo
            callableStatement.setInt(10, seizure.getSeizureInformation().getDebitForm()); // forma debito
            callableStatement.setInt(11, new Integer(1)); //tipo medida
            callableStatement.setInt(12, new Integer(seizure.getSeizureInformation().getEntity())); //identidad embargo
            callableStatement.setInt(13, new Integer(1)); //edo embargo
            callableStatement.setInt(14, new Integer(2)); //edo carga

            java.sql.Date theDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            java.sql.Date theDateAsString = java.sql.Date.valueOf(seizure.getSeizureInformation().getReceptionDate());

            callableStatement.setDate(15, theDateAsString);//fecha recepcion
            callableStatement.setString(16, "1"); //num oficio
            callableStatement.setString(17, seizure.getSeizureInformation().getProcessNumber()); //num proceso
            callableStatement.setString(18, seizure.getSeizureInformation().getJudicialAccount()); //cuenta judicial
            callableStatement.setInt(19, seizure.getSeizureInformation().getAmount()); //monto
            callableStatement.setString(20, "M"); //flag tipo carga
            callableStatement.setString(21, seizure.getSeizureInformation().getAddress()); //direccion
            callableStatement.setString(22, seizure.getSeizureInformation().getCity()); //ciudad
            callableStatement.setString(23, "mlvargas"); //usuario

            String demandantes = seizure.getSeizureInformation().getPlaintiff().getFirstName() + "," +
                                seizure.getSeizureInformation().getPlaintiff().getMiddleName() + "," +
                                seizure.getSeizureInformation().getPlaintiff().getSurname() + "," +
                                seizure.getSeizureInformation().getPlaintiff().getLastSurname() + "," +
                                seizure.getSeizureInformation().getPlaintiff().getDocument().getDocumentType() + "," +
                                seizure.getSeizureInformation().getPlaintiff().getDocument().getDocumentNumber();

            callableStatement.setString(24, demandantes); //demandantes

            String embProductos = seizure.getProduct().getProductID()  + "," +
                               seizure.getProduct().getProductState();

            callableStatement.setString(25, embProductos); //emb prod
            callableStatement.setString(26, null); //representante legal

            callableStatement.registerOutParameter(27, Types.INTEGER);
            callableStatement.registerOutParameter(28, Types.INTEGER);
            callableStatement.registerOutParameter(29, Types.VARCHAR);


            System.out.println("Ejecutando " + nameStoredProcedure);
            resultSet = callableStatement.executeQuery();
            System.out.println("Finaliza ejecucion " + nameStoredProcedure);

            Integer idSeizure = (Integer) callableStatement.getObject(27);
            System.out.println("Respuesta 27 " +  idSeizure);
            String codigoError = (String) callableStatement.getObject(28);
            System.out.println("Respuesta 28 " +  codigoError);
            String mensajeError = (String) callableStatement.getObject(29);
            System.out.println("Respuesta 29 " +  mensajeError);

            final ResponseSeizure responseSeizure = new ResponseSeizure(idSeizure, codigoError, mensajeError);
            return responseSeizure;

        } finally {
            if(resultSet != null) {
                resultSet.close();
            }

            if(callableStatement != null) {
                callableStatement.close();
            }
        }

    }
}



