package example.domain;

import io.micronaut.core.annotation.Creator;

import javax.persistence.Entity;

@Entity
public class Client {

    private String firstName;
    private String middleName;
    private String surname;
    private String lastSurname;
    private DocumentInfo document;

    @Creator
    public Client(String firstName, String middleName, String surname, String lastSurname, DocumentInfo document) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.surname = surname;
        this.lastSurname = lastSurname;
        this.document = document;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLastSurname() {
        return lastSurname;
    }

    public void setLastSurname(String lastSurname) {
        this.lastSurname = lastSurname;
    }

    public DocumentInfo getDocument() {
        return document;
    }

    public void setDocument(DocumentInfo document) {
        this.document = document;
    }
}
