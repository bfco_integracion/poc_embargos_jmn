package example.domain;

import io.micronaut.core.annotation.Creator;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Seizure {

    @Id
    private Integer id;
    private Client customer;
    private SeizeInfo seizureInformation;
    private ProductName product;

    public Seizure(Client customer, SeizeInfo seizureInformation, ProductName product) {
        this.customer = customer;
        this.seizureInformation = seizureInformation;
        this.product = product;
    }

    public Seizure(Integer id, Client customer, SeizeInfo seizureInformation, ProductName product) {
        this.id = id;
        this.customer = customer;
        this.seizureInformation = seizureInformation;
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getCustomer() {
        return customer;
    }

    public void setCustomer(Client customer) {
        this.customer = customer;
    }

    public SeizeInfo getSeizureInformation() {
        return seizureInformation;
    }

    public void setSeizureInformation(SeizeInfo seizureInformation) {
        this.seizureInformation = seizureInformation;
    }

    public ProductName getProduct() {
        return product;
    }

    public void setProduct(ProductName product) {
        this.product = product;
    }


}
